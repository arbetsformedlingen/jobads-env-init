import unittest
from unittest.mock import Mock, ANY

from job_ads_env_init.processes.subprocesses import create_os_account_and_k8s_secret


class CreateAccountInfoTest(unittest.TestCase):
    def test_create_account_creates_both_account_and_secret(self):
        k8s_conf = Mock()
        k8s_conf.create_or_replace_secret = Mock()
        k8s_conf.create_or_replace_secret.return_value = (True, "")
        os_conf = Mock()
        os_conf.create_user = Mock()
        os_conf.create_user.return_value = (True, "")

        result, _ = create_os_account_and_k8s_secret(
            k8s_conf,
            os_conf,
            "my_user",
            ["role1", "role2"],
            "my_secret",
            "OS_USER",
            "OS_PWD")
        os_conf.create_user.assert_called_with(ANY, ["role1", "role2"])
        k8s_conf.create_or_replace_secret.assert_called_with("my_secret", ANY)
        self.assertTrue(result)

    def test_create_account_stop_when_fail_create_account(self):
        k8s_conf = Mock()
        os_conf = Mock()
        os_conf.create_user = Mock()
        os_conf.create_user.return_value = (False, "error")

        result, msg = create_os_account_and_k8s_secret(
            k8s_conf,
            os_conf,
            "my_user",
            ["role1", "role2"],
            "my_secret",
            "OS_USER",
            "OS_PWD")
        os_conf.create_user.assert_called_with(ANY, ["role1", "role2"])
        self.assertFalse(result)
        self.assertEqual("error", msg)


if __name__ == '__main__':
    unittest.main()
