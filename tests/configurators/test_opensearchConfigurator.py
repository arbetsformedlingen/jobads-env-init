import os
import unittest
from unittest.mock import Mock

import pytest as pytest

import job_ads_env_init.configurators.opensearchConfigurator as osConfigurator


class OpenSearchMock:

    def __init__(self,
                 **kwargs,
                 ):
        pass


@pytest.mark.integration
class OpenSearchIntegrationTest(unittest.TestCase):
    configurator = osConfigurator.OpenSearchConfigurator(
        auth=osConfigurator.Auth(os.getenv("OS_USER", ""), os.getenv("OS_PWD", "")),
        verify_ssl=False)

    def test_create_user(self):
        user = osConfigurator.Auth("foo", "barGazonk12!")
        result, _ = self.configurator.create_user(user)
        self.assertEqual(True, result)

    def test_create_role(self):
        role = "MyRole"
        result, _ = self.configurator.create_role(role,
                                                  ["indices_monitor"],
                                                  [osConfigurator.IndexPermission(["myindex*"], ["read"])])
        self.assertEqual(True, result)


class OpenSearchTest(unittest.TestCase):
    requests_mock = Mock()
    configurator = osConfigurator.OpenSearchConfigurator(
        auth=osConfigurator.Auth("os_user", "os_pwd"),
        requests_module=requests_mock,
        opensearch_module=OpenSearchMock)

    args = None
    response = Mock()

    def test_create_user_success(self):
        def put(url, **kwargs):
            self.args = kwargs
            self.args['url'] = url
            self.response.status_code = 200
            return self.response

        auth = osConfigurator.Auth("dummy_user", "dummy_pwd")
        self.requests_mock.put = put
        result, _ = self.configurator.create_user(auth, roles=["my_role"])
        self.assertEqual("https://localhost:9243/_plugins/_security/api/internalusers/dummy_user", self.args['url'])
        self.assertDictEqual({
            "password": "dummy_pwd",
            "opendistro_security_roles": ["my_role"],
        }, self.args["json"])

        self.assertEqual(result, True)

    def test_create_user_fail(self):
        def put(url, **kwargs):
            self.args = kwargs
            self.args['url'] = url
            self.response.status_code = 300
            self.response.text = "error text"
            return self.response

        auth = osConfigurator.Auth("dummy_user", "dummy_pwd")
        self.requests_mock.put = put
        result, msg = self.configurator.create_user(auth)
        self.assertEqual(False, result)
        self.assertEqual("error text", msg)

    def test_create_role_success(self):
        def put(url, **kwargs):
            self.args = kwargs
            self.args['url'] = url
            self.response.status_code = 200
            return self.response

        self.requests_mock.put = put

        result, _ = self.configurator.create_role("my_role",
                                                  ["cluster_perm"],
                                                  [osConfigurator.IndexPermission(["my_index"], ["my_action"])])
        self.assertEqual(True, result)
        self.assertEqual("https://localhost:9243/_plugins/_security/api/roles/my_role", self.args['url'])
        self.assertDictEqual({
            "cluster_permissions": ["cluster_perm"],
            "index_permissions": [
                {"index_patterns": ["my_index"], "allowed_actions": ["my_action"]}
            ]
        },
            self.args['json'])

    def test_create_role_without_cluster_permissions(self):
        def put(url, **kwargs):
            self.args = kwargs
            self.args['url'] = url
            self.response.status_code = 200
            return self.response

        self.requests_mock.put = put

        result, _ = self.configurator.create_role("my_role",
                                                  [],
                                                  [osConfigurator.IndexPermission(["my_index"], ["my_action"])])
        self.assertEqual(True, result)
        self.assertEqual("https://localhost:9243/_plugins/_security/api/roles/my_role", self.args['url'])
        self.assertDictEqual({
            "index_permissions": [
                {"index_patterns": ["my_index"], "allowed_actions": ["my_action"]}
            ]
        },
            self.args['json'])

    def test_create_role_fail(self):
        def put(url, **kwargs):
            self.args = kwargs
            self.args['url'] = url
            self.response.status_code = 300
            self.response.text = "error text"
            return self.response

        self.requests_mock.put = put

        result, msg = self.configurator.create_role("my_role",
                                                    ["indices_monitor"],
                                                    [osConfigurator.IndexPermission(["myindex*"], ["read"])])
        self.assertEqual(False, result)
        self.assertEqual("error text", msg)


if __name__ == '__main__':
    unittest.main()
