import unittest
from unittest.mock import Mock

import kubernetes

import job_ads_env_init.configurators.k8sConfigurator as k


class K8sConfiguratorTest(unittest.TestCase):

    def test_create_or_replace_secret_when_creating_new(self):
        k8s_mock = Mock()
        k8s_mock.client = Mock()
        core_v1_api_mock = Mock()
        core_v1_api_mock.read_namespaced_secret = \
            Mock(side_effect=kubernetes.client.exceptions.ApiException(status=404, reason="expected error"))
        k8s_mock.client.CoreV1Api = lambda : core_v1_api_mock
        configurator = k.KubernetesConfigurator(k8s_module=k8s_mock)

        result, _ = configurator.create_or_replace_secret("secret_name", {"USER": "foo", "PWD": "bar"}, "my_namespace")
        core_v1_api_mock.create_namespaced_secret.assert_called_with("my_namespace",
                                                                     {
                                                                         'apiVersion': 'v1',
                                                                         'kind': 'Secret',
                                                                         'metadata': {'name': 'secret_name'},
                                                                         'stringData': {'USER': 'foo', 'PWD': 'bar'}
                                                                     })

        self.assertEqual(True, result)

    def test_create_or_replace_secret_when_updating(self):
        k8s_mock = Mock()
        k8s_mock.client = Mock()
        core_v1_api_mock = Mock()
        k8s_mock.client.CoreV1Api = lambda : core_v1_api_mock
        configurator = k.KubernetesConfigurator(k8s_module=k8s_mock)

        result, _ = configurator.create_or_replace_secret("secret_name", {"USER": "foo", "PWD": "bar"}, "my_namespace")
        core_v1_api_mock.replace_namespaced_secret.assert_called_with("secret_name",
                                                                      "my_namespace",
                                                                      {
                                                                          'apiVersion': 'v1',
                                                                          'kind': 'Secret',
                                                                          'metadata': {'name': 'secret_name'},
                                                                          'stringData': {'USER': 'foo', 'PWD': 'bar'}
                                                                      })

        self.assertEqual(True, result)

    def test_create_secret_fail_talk_to_k8s(self):
        core_v1_api_mock = Mock()
        core_v1_api_mock.create_namespaced_secret = \
            Mock(side_effect=kubernetes.client.exceptions.ApiException(status=200, reason="expected error"))
        core_v1_api_mock.read_namespaced_secret = \
            Mock(side_effect=kubernetes.client.exceptions.ApiException(status=404, reason="expected error"))
        k8s_mock = Mock()
        k8s_mock.client = Mock()
        k8s_mock.client.CoreV1Api = lambda : core_v1_api_mock
        configurator = k.KubernetesConfigurator(k8s_module=k8s_mock)

        result, msg = configurator.create_or_replace_secret("secret_name", {"USER": "foo", "PWD": "bar"}, "my_namespace")
        self.assertEqual(False, result)
        self.assertEqual("expected error", msg)

    def test_secret_exist_true(self):
        k8s_mock = Mock()
        k8s_mock.client = Mock()
        core_v1_api_mock = Mock()
        k8s_mock.client.CoreV1Api = lambda : core_v1_api_mock
        configurator = k.KubernetesConfigurator(k8s_module=k8s_mock)

        result = configurator.secret_exist("secret_name", "my_namespace")
        core_v1_api_mock.read_namespaced_secret.assert_called_with(name="secret_name", namespace="my_namespace")

        self.assertEqual(True, result)

    def test_secret_exist_false(self):
        core_v1_api_mock = Mock()
        core_v1_api_mock.read_namespaced_secret = \
            Mock(side_effect=kubernetes.client.exceptions.ApiException(status=404))
        k8s_mock = Mock()
        k8s_mock.client = Mock()
        k8s_mock.client.CoreV1Api = lambda : core_v1_api_mock
        configurator = k.KubernetesConfigurator(k8s_module=k8s_mock)

        result = configurator.secret_exist("secret_name", "my_namespace")
        core_v1_api_mock.read_namespaced_secret.assert_called_with(name="secret_name", namespace="my_namespace")

        self.assertEqual(False, result)

    def test_secret_exist_fail_talk_to_k8s(self):
        core_v1_api_mock = Mock()
        core_v1_api_mock.read_namespaced_secret = \
            Mock(side_effect=kubernetes.client.exceptions.ApiException(status=503))
        k8s_mock = Mock()
        k8s_mock.client = Mock()
        k8s_mock.client.CoreV1Api = lambda : core_v1_api_mock
        configurator = k.KubernetesConfigurator(k8s_module=k8s_mock)

        try:
            result = configurator.secret_exist("secret_name", "my_namespace")
        except kubernetes.client.exceptions.ApiException:
            return
        self.fail()


if __name__ == '__main__':
    unittest.main()
