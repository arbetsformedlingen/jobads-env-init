import unittest

from job_ads_env_init.utils.misc import create_password


class UtilsTests(unittest.TestCase):

    def test_create_password_create_valid_password(self):
        result = create_password()
        self.assertEqual(25, len(result))
        self.assertTrue(any(c.isdigit() for c in result))
        self.assertTrue(any(c.isalpha() and c.islower() for c in result))
        self.assertTrue(any(c.isalpha() and c.isupper() for c in result))
        self.assertTrue(any(c in "-,_~!&()*+.;%=" for c in result))

    def test_create_password_correct_length(self):
        result = create_password(10)
        self.assertEqual(10, len(result))


if __name__ == '__main__':
    unittest.main()
