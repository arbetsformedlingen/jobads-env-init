# Job ads environment init

Purpose of this repo is to automate setup of roles, users and permissions
in OpenSearch and corresponding needed secrets in Kubernetes to make
the Job ads services to run.

Initially this is only supporting historical-ads.

The software is idempotent, which means you can run it multiple times.
Though, password on users are updated each time and the secret changed
accordingly. This ease rotation of passwords.

## Prerequisites

* An OpenShift(Kubernetes) cluster setup
* An OpenSearch cluster setup. 
  [Instructions how to set it up in AWS](docs/install-opensearch-aws.md).
* Access from OpenShift to OpenSearch

Software can either be installed locally or within Kubernetes. The 
latter is recommended.

## Run in Kubernetes

To this repository, there is a corresponding repository with 
[job-ads-env-init-infra](https://gitlab.com/arbetsformedlingen/job-ads/job-ads-env-init-infra).
This repository provides kubernetes manifests using Kustomize to setup 
the needed config.

1. Create namespace, ensure it becomes your current namespace.
2. Create secret `historical-opensearch-access` with information about 
  OpenSearch admin user and OpenSearch host.
3. Apply the kustomize: 
   `kubectl apply -k kustomize/base`
4. A job is now launched setting up OpenSearch for Historical Ads.

You can verify the result. First ensure the job ran well, do `kubectl get jobs`.
You are expected to get a result like:

```text
NAME              COMPLETIONS   DURATION   AGE
historical-init   1/1           18s        87s
```

If it did not complete. Check the logs of the job.

Next, verify there is a Kubernetes secret `historical-writer`.

## Run locally

Ensure the desired namespace in Kubernetes is your current namespace.

```shell
python -m pip install --upgrade pip setuptools build
python -m build --outdir out
python -m pip install out/job_ads_env_init-0.1.0-py3-none-any.whl
```

You shall now be able to run the program `job-ads-env-init`.
Do `job-ads-env-init historical --help` to get help.

## Output

### In OpenSearch

In OpenSearch are following setup:
* Role `historical-write-role` with permission to manage indices `historical*`
* User `historical-write` bound to the role `historical-write-role`

### In Kubernetes

In Kubernetes are the following setup:
* A secret `historical-write` to be used by the historical-ads importer.
  The secret contains user and password to the user in OpenSearch.
