import random
import string


def create_password(length=25) -> str:
    chars = [random.choice(string.ascii_letters + string.digits + "-,_~!&()*+.;%=") for c in range(length-4)]
    # Guarantee at least on char of each type.
    chars.append(random.choice(string.ascii_lowercase))
    chars.append(random.choice(string.ascii_uppercase))
    chars.append(random.choice(string.digits))
    chars.append(random.choice("-,_~!&()*+.;%="))

    random.SystemRandom().shuffle(chars)
    return "".join(chars)
