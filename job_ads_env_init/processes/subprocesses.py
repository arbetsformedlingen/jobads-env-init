from job_ads_env_init.utils import misc
from job_ads_env_init.configurators import k8sConfigurator, opensearchConfigurator


def create_os_account_and_k8s_secret(k8s_configurator: k8sConfigurator,
                                     os_configurator: opensearchConfigurator,
                                     username: str,
                                     roles: list[str],
                                     secret_name: str,
                                     user_env: str = 'USER',
                                     password_env: str = 'PASSWORD') -> (bool, str):
    password = misc.create_password()
    result, message = os_configurator.create_user(opensearchConfigurator.Auth(username, password), roles)
    if not result:
        return result, message
    secret_data = {
        user_env: username,
        password_env: password
    }
    result, message = k8s_configurator.create_or_replace_secret(secret_name, secret_data)
    return result, message
