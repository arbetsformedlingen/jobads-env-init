import fire

from job_ads_env_init.configurators.opensearchConfigurator import OpenSearchConfigurator, Auth
from job_ads_env_init.processes.historical import historical_setup


class JobAdStorageInitCLI(object):

    def historical(self, user: str, password: str, host: str = "localhost", port: int = 443,
                   verify_ssl: bool = True) -> None:
        """
        Set up environment in OpenSearch for historical
        :param host: OpenSearch port
        :param port: OpenSearch port
        :param user: OpenSearch admin user
        :param password: OpenSearch admin password
        :param verify_ssl: Validate or not the SSL cert. Useful for local setup.
        :return:
        """
        os_configurator = OpenSearchConfigurator(host, port, Auth(user, password), verify_ssl)
        historical_setup(os_configurator)


def main():
    fire.Fire(JobAdStorageInitCLI)


if __name__ == '__main__':
    main()
