import logging

import kubernetes.client.exceptions
import kubernetes


class KubernetesConfigurator:

    def __init__(self,
                 k8s_module=kubernetes):
        self._k8s = k8s_module
        try:
            self._k8s.config.load_incluster_config()
            self._in_cluster = True
        except self._k8s.config.ConfigException:
            try:
                self._k8s.config.load_kube_config()
                self._in_cluster = False
            except self._k8s.config.ConfigException:
                raise Exception("Could not configure kubernetes python client")

    def get_current_namespace(self) -> str:
        if self._in_cluster:
            with open('/var/run/secrets/kubernetes.io/serviceaccount/namespace', 'r') as f:
                return f.read()
        else:
            return self._k8s.config.list_kube_config_contexts()[1]['context']['namespace']

    def create_or_replace_secret(self, name: str, values: dict[str, str], namespace: str = None) -> (bool, str):
        if namespace is None:
            namespace = self.get_current_namespace()
        body = {
            "apiVersion": "v1",
            "kind": "Secret",
            "metadata": {
                "name": name
            },
            "stringData": values
        }
        try:
            if self.secret_exist(name, namespace):
                response = self._k8s.client.CoreV1Api().replace_namespaced_secret(name, namespace, body)
            else:
                response = self._k8s.client.CoreV1Api().create_namespaced_secret(namespace, body)
        except kubernetes.client.exceptions.ApiException as e:
            logging.error(f"Failed create secret {namespace}.{name}: {e.reason}")
            return False, e.reason
        return True, ""

    def secret_exist(self, name: str, namespace: str = None) -> bool:
        if namespace is None:
            namespace = self.get_current_namespace()
        try:
            resp = self._k8s.client.CoreV1Api().read_namespaced_secret(name=name, namespace=namespace)
        except kubernetes.client.exceptions.ApiException as e:
            if e.status != 404:
                logging.error(f"Failed check if secret {namespace}.{name} exist: {e.reason}")
                raise e
            return False
        return True
