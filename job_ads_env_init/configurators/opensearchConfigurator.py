import logging

import opensearchpy
import requests
from collections import namedtuple

Auth = namedtuple("Auth", "user password")
Client = namedtuple("Client", "opensearch_client url auth insecure_ssl requests")
IndexPermission = namedtuple("IndexPermission", "index_patterns allowed_actions")  # list[string], list[string]


def _index_permission_mapper(ip: IndexPermission):
    return {"index_patterns": ip.index_patterns,
            "allowed_actions": ip.allowed_actions
            }


class OpenSearchConfigurator:

    def __init__(self,
                 host: str = "localhost",
                 port: int = 9243,
                 auth: Auth = Auth("", ""),
                 verify_ssl: bool = True,
                 requests_module=requests,
                 opensearch_module=opensearchpy.OpenSearch
                 ):
        self._os_url = f"https://{host}:{port}/"
        self._auth = auth
        self._verify_ssl = verify_ssl
        self._requests = requests_module
        self._client = opensearch_module(
            hosts=[{'host': host, 'port': port}],
            http_compress=True,
            http_auth=auth,
            use_ssl=True,
            verify_certs=verify_ssl,
            ssl_assert_hostname=verify_ssl
        )

    def create_user(self, new_user_auth: Auth, roles: list[str] = []) -> (bool, str):
        request_body = {
            "password": new_user_auth.password,
            "opendistro_security_roles": roles,
        }
        response = self._requests.put(f"{self._os_url}_plugins/_security/api/internalusers/{new_user_auth.user}",
                                      auth=self._auth,
                                      verify=self._verify_ssl,
                                      json=request_body)
        if 200 <= response.status_code < 300:
            logging.info(f"User {new_user_auth.user} updated.")
            return True, ""
        else:
            logging.error(f"Failed create user {new_user_auth.user}: {response.text}")
            return False, response.text

    def create_role(self,
                    role_name: str,
                    cluster_permissions: list[str],
                    index_permissions: list[IndexPermission]) -> (bool, str):
        request_body = {
            "index_permissions": [_index_permission_mapper(ip) for ip in index_permissions]
        }
        if len(cluster_permissions) > 0:
            request_body["cluster_permissions"] = cluster_permissions

        response = self._requests.put(f"{self._os_url}_plugins/_security/api/roles/{role_name}",
                                      auth=self._auth,
                                      verify=self._verify_ssl,
                                      json=request_body)
        if 200 <= response.status_code < 300:
            logging.info(f"Role {role_name} updated.")
            return True, ""
        else:
            logging.error(f"Failed create role {role_name}: {response.text}")
            return False, response.text
